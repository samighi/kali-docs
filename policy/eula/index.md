---
title: Kali Linux EULA
description:
icon:
type: post
weight:
author: ["g0tmi1k",]
---

For Kali Linux's End-User License Agreement (EULA), please see the following page: [kali.org/docs/policy/EULA.txt](/docs/policy/EULA.txt).
